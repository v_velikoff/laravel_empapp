<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeePostRequest;
use App\Models\Employee;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate(10);
        return  view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePostRequest $request)
    {
        // sanitize input
        $args = [
            'name' => FILTER_SANITIZE_STRING,
            'address' => FILTER_SANITIZE_STRING,
            'phone' => FILTER_SANITIZE_STRING,
            'department' => FILTER_SANITIZE_STRING,
            'position' => FILTER_SANITIZE_STRING,
            'salary' => FILTER_SANITIZE_NUMBER_FLOAT,
        ];
        $sanitizedReq = filter_var_array($request->validated(), $args);

        Employee::create($sanitizedReq);
        return redirect()->route('employee.index')->with('success', 'Employee added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //not implemented
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeePostRequest $request, Employee $employee)
    {
        // sanitize input
        $args = [
            'name' => FILTER_SANITIZE_STRING,
            'address' => FILTER_SANITIZE_STRING,
            'phone' => FILTER_SANITIZE_STRING,
            'department' => FILTER_SANITIZE_STRING,
            'position' => FILTER_SANITIZE_STRING,
            'salary' => FILTER_SANITIZE_NUMBER_FLOAT,
        ];
        $sanitizedReq = filter_var_array($request->validated(), $args);

        $employee->update($sanitizedReq);
        return redirect()->route('employee.index')
            ->with('success', 'Employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index')
            ->with('success', 'Employee deleted successfully');
    }
}
