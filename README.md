## About Laravel

1. Clone project
2. Run **composer install**
3. Run **npm install**
4. Create copy of .env file and set your DB
5. Generate an app encryption key - **php artisan key:generate**
6. Run **php artisan migrate**
7. Run **php artisan db:seed**
