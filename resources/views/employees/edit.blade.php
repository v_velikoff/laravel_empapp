@extends('dashboard')

@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                <div slot="avatar">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                        <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                        <line x1="12" y1="8" x2="12" y2="12"></line>
                        <line x1="12" y1="16" x2="12.01" y2="16"></line>
                    </svg>
                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    {{ $error }}
                </div>
            </div>
        @endforeach
    @endif

    <div class="pb-4"><a href="{{ route('employee.index') }}" class="ml-3 text-xl bg-green-500 hover:bg-green-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Back</a></div>
    <form id="editForm" action="{{ route('employee.update', $employee->id) }}" method="POST" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
        @csrf
        @method('PUT')
        <p class="text-gray-800 font-medium">Edit employee</p>
        <div class="mt-2 group">
            <label class="block text-sm text-gray-00" for="name">Name</label>
            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" value="{{ $employee->name }}" id="name" name="name" type="text" required="" placeholder="Name" aria-label="name">
        </div>
        <div class="mt-2 group">
            <label class=" block text-sm text-gray-600" for="address">Address</label>
            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" value="{{ $employee->address }}" id="address" name="address" type="text" required="" placeholder="Address" aria-label="address">
        </div>
        <div class="mt-2 group">
            <label class=" block text-sm text-gray-600" for="phone">Phone #</label>
            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" value="{{ $employee->phone }}" id="phone" name="phone" required="" placeholder="Phone number" aria-label="phone">
        </div>
        <div class="mt-2 group">
            <label class=" block text-sm text-gray-600" for="department">Department</label>
            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" value="{{ $employee->department }}" id="department" name="department" type="text" required="" placeholder="Department" aria-label="department">
        </div>
        <div class="mt-2 group">
            <label class=" block text-sm text-gray-600" for="position">Position</label>
            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" value="{{ $employee->position }}" id="position" name="position" type="text" required="" placeholder="Position" aria-label="position">
        </div>
        <div class="mt-2 group">
            <label class=" block text-sm text-gray-600" for="salary">Salary</label>
            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" value="{{ $employee->salary }}" id="salary" name="salary" required="" placeholder="Salary" aria-label="salary">
        </div>
        <div class="mt-4">
            <button type="submit" class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Edit employee</button>
        </div>
    </form>
@endsection
@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#editForm').validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 255,
                    },
                    address: {
                        required: true,
                        maxlength: 255,
                    },
                    phone: {
                        required: true,
                        maxlength: 255,
                    },
                    department: {
                        required: true,
                        maxlength: 255,
                    },
                    position: {
                        required: true,
                        maxlength: 255,
                    },
                    salary: {
                        required: true,
                        number: true,
                        min: 0
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('text-red-600');
                    element.closest('.group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('bg-red-600');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('bg-red-600');
                }
            });
        });
    </script>
@endsection
