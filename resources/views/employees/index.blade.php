@extends('dashboard')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <div class="flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-green-700 bg-green-100 border border-green-300 ">
                <div slot="avatar">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle w-5 h-5 mx-2">
                        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                        <polyline points="22 4 12 14.01 9 11.01"></polyline>
                    </svg>
                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    {{ $message }}
                </div>
            </div>
        </div>
    @endif
    <div class="pb-4"><a href="{{ route('employee.create') }}" class="float-right mr-3 text-sm bg-green-500 hover:bg-green-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Add Employee</a></div>
    <table class="w-full text-md bg-white shadow-md rounded mb-4">
        <thead>
        <tr class="border-b">
            <th class="text-left p-3 px-5">Name</th>
            <th class="text-left p-3 px-5">Address</th>
            <th class="text-left p-3 px-5">Phone #</th>
            <th class="text-left p-3 px-5">Department</th>
            <th class="text-left p-3 px-5">Position</th>
            <th class="text-left p-3 px-5">Salary</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
        <tr class="border-b hover:bg-orange-100 bg-gray-100">
            <td class="p-3 px-5">{{ $employee->name }}</td>
            <td class="p-3 px-5">{{ $employee->address }}</td>
            <td class="p-3 px-5">{{ $employee->phone }}</td>
            <td class="p-3 px-5">{{ $employee->department }}</td>
            <td class="p-3 px-5">{{ $employee->position }}</td>
            <td class="p-3 px-5">{{ $employee->salary }}</td>
            <td class="p-3 px-5 flex justify-end">
                <a title="edit" href="{{ route('employee.edit', $employee->id) }}" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Edit</a>
                <form action="{{ route('employee.destroy', $employee->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" title="delete" class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    {!! $employees->links() !!}
@endsection

